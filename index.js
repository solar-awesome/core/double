const SolarRPC = require("@solar/core-grpc");
const SolarHTTP = require("@solar/core-http");

/**
 * Represents both gRPC and HTTP services
 * @class
 */
class SolarDouble {

	/**
	 * Creates instance of SolarDouble.
	 * @constructor
	 * @param {Object=} config.http HTTP config
	 * @param {Object} config.grpc gRPC config
	 * @param {String} config.protoPath Path to .proto file
	 * @param {Number=} config.http.port Port, default: process.env.HTTP_PORT || 3000
	 * @param {String=} config.http.host Host, default: process.env.HTTP_HOST || "localhost"
	 * @param {String=} config.http.endpoint Url for GET endpoint that shares .proto file, default: "/proto"
	 * @param {Object} config.grpc.implementations Object of implementations
	 * @param {Object=} config.grpc.loggerOptions Logger options, see {@link https://github.com/malijs/logger#loggeroptions|@malijs/logger}
	 */
	constructor(config) {
		this.config = config;
	}

	/**
	 * Starts SolarDouble service.
	 */
	async start() {
		const httpService = new SolarHTTP({
			...this.config.http,
			protoPath: this.config.protoPath
		});
		httpService.start();

		const grpcService = new SolarRPC({
			...this.config.grpc,
			protoPath: this.config.protoPath
		});
		grpcService.start();

		return grpcService;
	}
}

module.exports = SolarDouble;